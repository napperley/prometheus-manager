package org.digieng.promMan.model

import kotlinx.serialization.Serializable

@Serializable
data class LabelsResponse(val status: String, val data: ArrayList<String>)
