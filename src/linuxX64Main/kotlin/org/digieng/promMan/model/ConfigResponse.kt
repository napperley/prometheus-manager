package org.digieng.promMan.model

import kotlinx.serialization.Serializable

@Serializable
data class ConfigResponse(val status: String, val data: ConfigData)
