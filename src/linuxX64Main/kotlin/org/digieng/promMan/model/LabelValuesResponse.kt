package org.digieng.promMan.model

import kotlinx.serialization.Serializable

@Serializable
data class LabelValuesResponse(val status: String, val data: ArrayList<String>)
