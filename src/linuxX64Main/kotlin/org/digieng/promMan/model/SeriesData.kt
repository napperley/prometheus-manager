package org.digieng.promMan.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SeriesData(@SerialName("__name__") val name: String, val job: String, val instance: String)
