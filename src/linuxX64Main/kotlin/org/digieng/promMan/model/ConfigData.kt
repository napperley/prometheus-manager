package org.digieng.promMan.model

import kotlinx.serialization.Serializable

@Serializable
data class ConfigData(val yaml: String)
