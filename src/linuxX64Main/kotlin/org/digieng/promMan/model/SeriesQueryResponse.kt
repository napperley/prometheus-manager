package org.digieng.promMan.model

import kotlinx.serialization.Serializable

@Serializable
data class SeriesQueryResponse(val status: String, val data: ArrayList<SeriesData>)
