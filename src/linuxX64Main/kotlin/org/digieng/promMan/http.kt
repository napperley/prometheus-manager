package org.digieng.promMan

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.withTimeout
import org.digieng.promMan.model.*

// HTTP timeout in ms.
private const val HTTP_TIMEOUT = 15000L
internal val httpClient = HttpClient {
    install(JsonFeature) {
        serializer = KotlinxSerializer()
    }
}

internal suspend fun HttpClient.callQuitRoute(serverUrl: String) {
    try {
        withTimeout(HTTP_TIMEOUT) {
            get<Unit>("$serverUrl/-/quit")
        }
    } catch (illegalState: IllegalStateException) {
        println("Quit failed: ${illegalState.message}")
    }
}

internal suspend fun HttpClient.callReadyRoute(serverUrl: String): HttpStatusCode {
    var result = HttpStatusCode.InternalServerError
    try {
        withTimeout(HTTP_TIMEOUT) {
            result = get<HttpResponse>("$serverUrl/-/ready").status
        }
    } catch (illegalState: IllegalStateException) {
        println("Getting ready status failed: ${illegalState.message}")
    }
    return result
}

internal suspend fun HttpClient.callHealthyRoute(serverUrl: String): HttpStatusCode {
    var result = HttpStatusCode.InternalServerError
    try {
        withTimeout(HTTP_TIMEOUT) {
            result = get<HttpResponse>("$serverUrl/-/healthy").status
        }
    } catch (illegalState: IllegalStateException) {
        println("Getting health status failed: ${illegalState.message}")
    }
    return result
}

internal suspend fun HttpClient.callListLabelsRoute(serverUrl: String): LabelsResponse {
    var resp = LabelsResponse("", ArrayList())
    try {
        withTimeout(HTTP_TIMEOUT) {
            resp = get("$serverUrl/api/v1/labels")
        }
    } catch (illegalState: IllegalStateException) {
        println("Listing labels failed: ${illegalState.message}")
    }
    return resp
}

internal suspend fun HttpClient.callListLabelValuesRoute(labelName: String, serverUrl: String): LabelValuesResponse {
    var resp = LabelValuesResponse("", ArrayList())
    try {
        withTimeout(HTTP_TIMEOUT) {
            resp = get("$serverUrl/api/v1/label/$labelName/values")
        }
    } catch (illegalState: IllegalStateException) {
        println("Label values query failed: ${illegalState.message}")
    }
    return resp
}

internal suspend fun HttpClient.callConfigRoute(serverUrl: String): ConfigResponse {
    var resp = ConfigResponse("", ConfigData(""))
    try {
        withTimeout(HTTP_TIMEOUT) {
            resp = get("$serverUrl/api/v1/status/config")
        }
    } catch (illegalState: IllegalStateException) {
        println("Getting configuration failed: ${illegalState.message}")
    }
    return resp
}

internal suspend fun HttpClient.callSeriesQueryRoute(selector: String, serverUrl: String): SeriesQueryResponse {
    var resp = SeriesQueryResponse("", ArrayList())
    try {
        withTimeout(HTTP_TIMEOUT) {
            resp = get("$serverUrl/api/v1/series?match[]=$selector")
        }
    } catch (illegalState: IllegalStateException) {
        println("Series query failed: ${illegalState.message}")
    }
    return resp
}

internal suspend fun HttpClient.callReloadRoute(serverUrl: String) {
    try {
        withTimeout(HTTP_TIMEOUT) {
            post<Unit>("$serverUrl/-/reload")
        }
    } catch (illegalState: IllegalStateException) {
        println("Server reload failed: ${illegalState.message}")
    }
}

internal suspend fun HttpClient.callHomeRoute(serverUrl: String): String {
    var resp = ""
    try {
        withTimeout(HTTP_TIMEOUT) {
            resp = get("$serverUrl/")
        }
    } catch (illegalState: IllegalStateException) {
        println("Server request failed: ${illegalState.message}")
    }
    return resp
}