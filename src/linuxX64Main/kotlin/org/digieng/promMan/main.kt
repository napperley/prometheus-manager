package org.digieng.promMan

import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

private const val PROGRAM_USAGE_TXT =
    """
        Prometheus Manager Usage
        ---------------------------
        * Default server response: prom_man <serverUrl>
        * Reload server: prom_man --reload <serverUrl>
        * Shutdown server: prom_man --shutdown <serverUrl>
        * Health status: prom_man --health-status <serverUrl>
        * Ready status: prom_man --ready-status <serverUrl>
        * Configuration status: prom_man --config-status <serverUrl>
        * List labels: prom_man --list-labels <serverUrl>
        * Series query: prom_man --series-query <selector> <serverUrl>
        * List label values: prom_man --label-values <label> <serverUrl>
    """

fun main(args: Array<String>) = runBlocking<Unit> {
    val serverUrl = if (args.isNotEmpty()) args.last() else ""
    if (args.size == 1) printHomeResponse(serverUrl)
    else if (args.size == 2 && args.first() == "--reload") reloadServer(serverUrl)
    else if (args.size == 2 && args.first() == "--shutdown") shutdownServer(serverUrl)
    else if (args.size == 2 && args.first() == "--health-status") printHealthStatus(serverUrl)
    else if (args.size == 2 && args.first() == "--ready-status") printReadyStatus(serverUrl)
    else if (args.size == 2 && args.first() == "--config-status") printConfig(serverUrl)
    else if (args.size == 2 && args.first() == "--list-labels") listLabels(serverUrl)
    else if (args.size == 3 && args.first() == "--series-query") runSeriesQuery(args[1], serverUrl)
    else if (args.size == 3 && args.first() == "--label-values") runListLabelValues(args[1], serverUrl)
    else println(PROGRAM_USAGE_TXT.trimIndent())
}

private fun CoroutineScope.shutdownServer(serverUrl: String) = launch {
    println("Shutting down server (on $serverUrl)...")
    httpClient.callQuitRoute(serverUrl)
}

private fun CoroutineScope.printConfig(serverUrl: String) = launch {
    println("Getting configuration (on $serverUrl)...")
    val resp = httpClient.callConfigRoute(serverUrl)
    if (resp.status == "success") println("Configuration:\n${resp.data.yaml}")
    else println("Configuration not loaded.")
}

private fun CoroutineScope.printReadyStatus(serverUrl: String) = launch {
    println("Getting ready status (on $serverUrl)...")
    val status = httpClient.callReadyRoute(serverUrl)
    if (status == HttpStatusCode.OK) println("Server ready.")
    else println("Server not ready: ${status.description} (${status.value}).")
}

private fun CoroutineScope.printHealthStatus(serverUrl: String) = launch {
    println("Getting health status (on $serverUrl)...")
    val status = httpClient.callHealthyRoute(serverUrl)
    if (status == HttpStatusCode.OK) println("Server healthy.")
    else println("Server not healthy: ${status.description} (${status.value}).")
}

private fun CoroutineScope.listLabels(serverUrl: String) = launch {
    println("Listing all labels (on $serverUrl)...")
    val labels = httpClient.callListLabelsRoute(serverUrl)
    println("Total Labels: ${labels.data.size}")
    if (labels.data.isNotEmpty()) labels.data.forEach { println("* $it") }
}

private fun CoroutineScope.runListLabelValues(label: String, serverUrl: String) = launch {
    println("Listing values for $label label (on $serverUrl)...")
    val labelValues = httpClient.callListLabelValuesRoute(label, serverUrl)
    println("Total label values: ${labelValues.data.size}")
    if (labelValues.data.isNotEmpty()) labelValues.data.forEach { println("* $it") }
}

private fun CoroutineScope.runSeriesQuery(selector: String, serverUrl: String) = launch {
    println("Querying series by $selector selector (on $serverUrl)...")
    val series = httpClient.callSeriesQueryRoute(selector, serverUrl)
    println("Total series items: ${series.data.size}")
    if (series.data.isNotEmpty()) series.data.forEach { println("* $it") }
}

private fun CoroutineScope.reloadServer(serverUrl: String) = launch {
    println("Reloading server (on $serverUrl)...")
    httpClient.callReloadRoute(serverUrl)
}

private fun CoroutineScope.printHomeResponse(serverUrl: String) = launch {
    println("Making request to server (on $serverUrl)...")
    val resp = httpClient.callHomeRoute(serverUrl)
    if (resp.isNotEmpty()) println("Response:\n$resp")
}
