group = "org.digieng"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.61"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.3.61"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    val username = properties["username"]
    val serverHost = properties["serverHost"]
    val serverDestDir = properties["serverDestDir"]
    val programArgs = fetchProgramArguments()
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val ktorClientVer = "1.3.0"
                implementation("io.ktor:ktor-client-curl:$ktorClientVer")
                implementation("io.ktor:ktor-client-serialization-native:$ktorClientVer")
            }
        }
        binaries {
            executable("prom_man") {
                runTask?.args(*programArgs)
                entryPoint = "org.digieng.promMan.main"
            }
        }
    }

    val compressLinuxX64ReleaseBinary by tasks.creating(Exec::class) {
        dependsOn("linkProm_manReleaseExecutableLinuxX64")
        val binaryDir = "$buildDir/bin/linuxX64/prom_manReleaseExecutable"
        val binaryFile = File("$binaryDir/prom_man.kexe")
        if (binaryFile.exists()) binaryFile.copyTo(target = File("$binaryDir/prom_man"), overwrite = true)
        commandLine("gzip", "-f", "-9", "$binaryDir/prom_man")
    }
    val copyLinuxX64ReleaseBinary by tasks.creating(Exec::class) {
        dependsOn(compressLinuxX64ReleaseBinary)
        val binaryDir = "$buildDir/bin/linuxX64/prom_manReleaseExecutable"
        commandLine("scp", "$binaryDir/prom_man.gz", "$username@$serverHost:$serverDestDir")
    }
    val removeRemoteLinuxX64ReleaseBinary by tasks.creating(Exec::class) {
        dependsOn(copyLinuxX64ReleaseBinary)
        commandLine("ssh", "-t", "$username@$serverHost", "rm", "-f", "$serverDestDir/prom_man")
    }
    val decompressRemoteLinuxX64ReleaseBinary by tasks.creating(Exec::class) {
        dependsOn(removeRemoteLinuxX64ReleaseBinary)
        commandLine("ssh", "-t", "$username@$serverHost", "gunzip", "-f", "$serverDestDir/prom_man.gz")
    }
    val alterRemoteLinuxX64ReleaseBinary by tasks.creating(Exec::class) {
        dependsOn(decompressRemoteLinuxX64ReleaseBinary)
        commandLine("ssh", "-t", "$username@$serverHost", "chmod", "+x", "$serverDestDir/prom_man")
    }
    tasks.create<Exec>("runRemoteLinuxX64ReleaseBinary") {
        dependsOn(alterRemoteLinuxX64ReleaseBinary)
        commandLine("ssh", "-t", "$username@$serverHost", "$serverDestDir/prom_man", *programArgs)
    }
}

fun fetchProgramArguments(): Array<String> {
    val tmp = mutableListOf<String>()
    File("program_args.txt").forEachLine { tmp += it }
    return tmp.toTypedArray()
}
